class PushService

  def initialize(user)
    @user = user
  end

  def send(message)
    return if @user.device_type.blank? || @user.device_token.blank?

    if (@user.device_type == "ios")
      sendAPN(message)
    elsif (@user.device_type == "android")
      sendGCM(message)
    end
  end

  private

  def sendAPN(message)
    apn_client = Rails.env.production? ? Houston::Client.production : Houston::Client.development
    pem_file = Rails.env.production? ? 'production_com.example.pem' : 'development_com.example.pem'
    pem_file_path = File.join(Rails.root, 'lib', 'assets', pem_file)
    apn_client.certificate = File.read(pem_file_path)
    notification = Houston::Notification.new(device: @user.device_token)
    notification.alert = message
    apn_client.push(notification)
  end

  def sendGCM(message)
    @gcm = GCM.new(ENV['GOOGLE_API_KEY'])
    registration_ids= [@user.device_token]
    options = {data: {message: message}}
    response = @gcm.send(registration_ids, options)
  end

end
