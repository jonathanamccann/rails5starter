class CreditCardService
  attr_reader :errors

  def initialize(user)
    @user = user
    @errors = []
  end

  def create_card(card_params)
    card_params = card_params.merge({object: 'card', number: card_params[:card_number], name: card_params[:name_on_card] })
    card_params.delete('card_number')
    card_params.delete('name_on_card')

    begin

      if( @user.stripe_customer_id.blank? )
        stripe_customer = Stripe::Customer.create(email: @user.email, description: @user.full_name)
        @user.update_attributes(stripe_customer_id: stripe_customer.id)
      else
        stripe_customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
      end

      stripe_token = Stripe::Token.create(card: card_params)
      stripe_card = stripe_customer.sources.create({:source => stripe_token.id})
      return save_card(stripe_card)
    rescue Stripe::StripeError => e
      @errors.push(e.message)
      #Rollbar.info "Stripe::Customer - StripeError", response: e.json_body
      return nil
    end
  end

  def save_card(stripe_card)
    credit_card = CreditCard.new(
        user_id: @user.id,
        stripe_id: stripe_card.id,
        last_4_digits: stripe_card.last4,
        exp_month: stripe_card.exp_month,
        exp_year: stripe_card.exp_year,
        name_on_card: stripe_card.name,
        card_type: stripe_card.brand,
        cvc_check: stripe_card.cvc_check,
        card_number: 'REDACTED',
        cvc: 'REDACTED'
    )

    credit_card.save!
    credit_card
  end

  def delete_card(credit_card)
    begin
      stripe_customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
      return stripe_customer.sources.retrieve(credit_card.stripe_id).delete()
    rescue Stripe::StripeError => e
      @errors.push(e.message)
      #Rollbar.info "Stripe::Customer - StripeError", response: e.json_body
      return nil
    end
  end

  #
  #  The plan is the string you enter into Stripe (ex. PRO_MONTHLY)
  #
  def subscribe(plan)
    begin
      stripe_customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
      stripe_subscription = stripe_customer.subscriptions.create(plan: plan)
      return stripe_subscription
    rescue Stripe::StripeError => e
      @errors.push(e.message)
      return nil
    end
  end

  def errors?
    @errors.length > 0
  end

  def copy_errors( credit_card )
    @errors.each do |error_message|
      credit_card.errors.add(:base, :issue_with_card, message: error_message)
    end

    credit_card
  end

end
