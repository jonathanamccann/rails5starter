class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)

    if( cookies[:utm_source].present? )
      resource.update_attribute(:source, cookies[:utm_source])
      cookies.delete :utm_source
    end

    if( resource.sign_in_count == 1 )
      thankyou_path
    else
      root_path
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:email, :password, :password_confirmation, :full_name)
    end
  end

  def verify_admin
    unless user_signed_in? && current_user.admin?
      redirect_to root_path, flash: {alert: "You don't have enough permissions to proceed"}
      return false
    end

    return true
  end

  def admin_layout
    "admin"
  end
end
