class Api::V1::BaseController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_filter :authenticate_with_token

  def authenticate_with_token
    @current_user = User.find_by_auth_token(request.headers['x-api-token'])

    unless @current_user
      render status: :unauthorized, json: {error: 'Invalid Credentials'}
    end
  end

  def render_errors_for(model, status = :unprocessable_entity)
    render json: {errors: model.errors.messages}, status: status
  end

  def resource_not_found( resource_name )
    render json: {error: resource_name + ' not found'}, :status => 404
  end
end
