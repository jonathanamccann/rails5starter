class Api::V1::UsersController < Api::V1::BaseController

  before_action :set_user

  def update
    if @user.update(user_params)
      render json: @user
    else
      render_errors_for(@user)
    end
  end

  private

  def user_params
    params.require(:user).permit(:device_type, :device_token)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
