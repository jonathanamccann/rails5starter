class Admin::UsersController < ApplicationController

  before_action :verify_admin
  layout :admin_layout

  def index
    @users = User.all
  end

  def send_message
    service = PushService.new(@user)
    service.send("Look ma ... Push notifications!!")
    redirect_to users_path
  end

end
