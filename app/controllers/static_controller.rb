class StaticController < ApplicationController

  before_action :verify_admin, only: [:admin]
  layout :get_layout

  def index
    if (static_params[:utm_source].present?)
      cookies[:utm_source] = params[:utm_source]
    end
  end

  def admin
  end

  def thankyou
  end

  private

  def get_layout
    case action_name
      when "admin"
        "admin"
      when "thankyou"
        "empty"
      else
        "application"
    end
  end

  def static_params
    params.permit(:utm_source)
  end
end
