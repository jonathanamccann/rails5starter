class DesignsController < ApplicationController

  layout :get_layout

  def paper
  end

  private

  def get_layout
    case action_name
      when "paper"
        "paper"
      else
        "application"
    end
  end
end
