module ApplicationHelper

  def facebook_share_link
    share_url = "https://www.facebook.com/sharer/sharer.php?"
    site_param = "u=" + url_encode( "http://www.DOMAIN_HERE.com" )
    title_param = "&t=" + url_encode( "Join a growing community!" )
    share_url + site_param + title_param
  end

  def twitter_share_link
    share_url = "https://twitter.com/intent/tweet?"
    title_param = "text=" + "Join a growing community!! "
    site_param = url_encode( "http://www.DOMAIN_HERE.com" )
    share_url + title_param + site_param
  end

end
