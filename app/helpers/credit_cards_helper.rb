module CreditCardsHelper

  def card_icon(credit_card)
    # Visa, American Express, MasterCard, Discover, JCB, Diners Club, or Unknown.
    if (credit_card.card_type == 'Visa')
      return 'fa-cc-visa'
    elsif (credit_card.card_type == 'American Express')
      return 'fa-cc-amex'
    elsif (credit_card.card_type == 'MasterCard')
      return 'fa-cc-mastercard'
    elsif (credit_card.card_type == 'Discover')
      return 'fa-cc-discover'
    else
      return 'fa-credit-card'
    end
  end

  def card_mask( credit_card )
    return 'xxxx-xxxx-xxxx-' + credit_card.last_4_digits
  end

  def expiration_date( credit_card )
    if( credit_card.exp_month < 10 )
      return '0' + credit_card.exp_month.to_s + '/' + credit_card.exp_year.to_s
    end

    return credit_card.exp_month.to_s + '/' + credit_card.exp_year.to_s
  end
end
