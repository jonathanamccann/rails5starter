class UserMailer < ApplicationMailer
  default from: 'you@example.com'

  def welcome(user)
    @user = user
    recipients = []
    recipients << user.email
    subject = 'Welcome'
    mail to: recipients, subject: subject
  end

end