# == Schema Information
#
# Table name: credit_cards
#
#  id               :integer          not null, primary key
#  name_on_card     :string
#  card_number      :string
#  expiration_month :integer
#  expiration_year  :integer
#  cvv              :string
#  zip_code         :string
#  card_type        :string
#  stripe_id        :string
#  last_4_digits    :string
#  user_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class CreditCard < ApplicationRecord
  belongs_to :user

  validates_presence_of :name_on_card
  validates_presence_of :card_number
  validates_presence_of :exp_month
  validates_presence_of :exp_year
  validates_presence_of :cvc
end
