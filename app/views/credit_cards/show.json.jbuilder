json.extract! @credit_card, :id, :name_on_card, :card_number, :expiration_month, :expiration_year, :cvv, :zip_code, :card_type, :user_id, :created_at, :updated_at
