json.array!(@credit_cards) do |credit_card|
  json.extract! credit_card, :id, :name_on_card, :card_number, :expiration_month, :expiration_year, :cvv, :zip_code, :card_type, :user_id
  json.url credit_card_url(credit_card, format: :json)
end
