Rails.application.routes.draw do

  get 'designs/paper'

  resources :credit_cards, path: '/payments'
  devise_for :users, :path_names => {:sign_in => 'signin', :sign_up => 'register', :sign_out => 'logout'},
             :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  root 'static#index'

  scope module: 'admin', path: '/admin' do
    resources :users
    get 'users/:id/message',  to: 'users#send_message', as: 'test_push_message'
  end

  get 'admin',  to: 'static#admin', as: 'admin_panel'
  get 'thankyou',  to: 'static#thankyou', as: 'thankyou'

  namespace :api do
    namespace :v1, format: :json do
      devise_scope :user do
        post 'login', to: 'sessions#create'
        post 'sign_up', to: 'registrations#create'
        get 'logout', to: 'sessions#destroy'
      end
    end
  end

end
