push_notifications_enabled = false

if (push_notifications_enabled && Rails.env.development?)
  APN = Houston::Client.development
  dev_pem_file = File.join(Rails.root, 'lib', 'assets', 'development_com.example.pem')
  APN.certificate = File.read(dev_pem_file)
elsif (push_notifications_enabled && Rails.env.production?)
  APN = Houston::Client.production
  prod_pem_file = File.join(Rails.root, 'lib', 'assets', 'production_com.example.pem')
  APN.certificate = File.read(prod_pem_file)
end
