# SAAS Starter

### It includes:

1. User Authentication
2. Email Templates
3. Payments
4. Bootstrap and FontAwesome
5. Admin Dashboard
6. Desktop Push Notifications
7. Mobile Push Notifications
8. API
9. Fonts


### After cloning this repo, you'll want to delete the .git and .idea files. Also, change your database.yml file to reflect your new project name.

### For Android Based Push notifications to function please set your Google API Key:

ENV['GOOGLE_API_KEY']

### For iOS Based Push notifications to function include your development and/or production PEM files under RAILS_ROOT/lib/assets

See RAILS_ROOT/app/services/push_service.rb for more details

### To setup Rollbar:
```

Uncomment the following lines from gemfile.rb:
#gem 'rollbar'
#gem 'oj', '~> 2.12.14'

Then, run the following command from your Rails root:

$ rails generate rollbar POST_SERVER_ITEM_ACCESS_TOKEN
Be sure to replace POST_SERVER_ITEM_ACCESS_TOKEN with your project's post_server_item access token, which you can find in the Rollbar.com interface.

That will create the file config/initializers/rollbar.rb, which initializes Rollbar and holds your access token and other configuration values.

If you want to store your access token outside of your repo, run the same command without arguments and create an environment variable ROLLBAR_ACCESS_TOKEN that holds your server-side access token:

$ rails generate rollbar
$ export ROLLBAR_ACCESS_TOKEN=POST_SERVER_ITEM_ACCESS_TOKEN
For Heroku users:

If you're on Heroku, you can store the access token in your Heroku config:

$ heroku config:add ROLLBAR_ACCESS_TOKEN=POST_SERVER_ITEM_ACCESS_TOKEN
```

### To setup Sidekiq:

Uncomment the following line from gemfile.rb:
#gem sidekiq

Run bundle install.

If you are using sidekiq (and redis, since sidekiq uses redis behind the scenes) I would suggest altering your Procfile to the following:
```
web: bundle exec puma -C config/puma.rb
redis: redis-server
worker: bundle exec sidekiq
```

Then, run `gem install foreman`. Be careful not to include `foreman` in your actual gemfile.

To run your app now, instead of `rails s`, use `foreman start`. This will run redis, sidekiq, and your rails server all in one terminal window.

